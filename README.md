# Car Management Dashboard
Sebuah Dashboard Manajemen Mobil untuk memanajemen data mobil dengan HTTP Server yang dilakukan melalui REST API (Postman). <br>
Manajemen data mobil meliputi aksi-aksi berikut : Menambahkan data mobil, Memodifikasi data mobil yang sudah ada, Menghapus data mobil yang sudah ada, dan Melihat daftar mobil yang tersedia di dalam database

## How to run
guide 

```bash
yarn sequelize-cli init
change username,password,database,host,dialect in development at config.json
yarn sequelize-cli db:create
yarn sequelize-cli model:generate --name Sizes --attributes name:string --underscored
yarn sequelize-cli model:generate --name Cars --attributes name:string,price:integer,size_id:integer,photo:string --underscored
set table name into lowercase
yarn migrate
```

## Endpoints
GET("/api/v1/cars") : untuk mendapatkan semua data mobil <br>
GET("/api/v1/cars/:id") : untuk mendapatkan data mobil berdasarkan id <br>
POST("/api/v1/cars") : untuk menampilkan data mobil <br>
POST("/api/v1/cars-upload") : untuk menampilkan data mobil melalui form <br>
PUT("/api/v1/cars/:id") : untuk memperbarui data mobil <br>
DELETE("/api/v1/cars/:id") : untuk menghapus data mobil

## Directory Structure

```
.gitignore
├── config
│   └── config.json
├── controllers
│   └── car_delete.js
│   └── car_get_all.js
│   └── car_get_id.js
│   └── car_get.js
│   └── car_post.js
│   └── car_put.js
│   └── car_upload.js
│   └── form.js
│   └── index.js
│   └── list.js
├── migrations
├── models
│   └── cars.js
│   └── index.js
│   └── sizes.js
├── node_modules
├── public
│   ├── assets
│   │   ├── css
│   │   ├── img
│   │   ├── js
├── upload
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   │   └── listcars.ejs
│   │   └── listforms.ejs
│   │   └── navbar.ejs
│   │   └── scripts.ejs
│   │   └── sidebar.ejs
│   ├── form.ejs
│   └── list.ejs
├── index.js
├── package.json
├── README.md
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/assets/img/erd.png)
